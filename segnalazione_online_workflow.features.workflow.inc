<?php

/**
 * Implementation of hook_workflow_defaults().
 */
function segnalazione_online_workflow_workflow_defaults() {
 $defaults = array();
  $defaults[] = array(
    'name' => 'Richiesta Workflow',
    'machine_name' => 'richiesta_workflow',
    'tab_roles' => array(
      '0' => 'admin',
      '1' => 'Community manager',
      '2' => 'manager',
      '3' => 'Operatore',
    ),
    'options' => array(
      'comment_log_node' => 1,
      'comment_log_tab' => 1,
      'name_as_title' => 0,
    ),
    'states' => array(
      '8' => array(
        'state' => '(creation)',
        'weight' => '-50',
        'sysid' => '1',
        'status' => '1',
        'module' => 'segnalazione_online_workflow',
        'ref' => '8',
        'access' => array(),
      ),
      '1344414387' => array(
        'state' => 'Sottomessa',
        'weight' => '0',
        'sysid' => '0',
        'status' => '1',
        'module' => 'segnalazione_online_workflow',
        'ref' => '1344414387',
        'access' => array(
          '0' => array(
            'role' => 'Unica',
            'grant_view' => '0',
            'grant_update' => '0',
            'grant_delete' => '0',
          ),
          '1' => array(
            'role' => 'Unibo',
            'grant_view' => '0',
            'grant_update' => '0',
            'grant_delete' => '0',
          ),
          '2' => array(
            'role' => 'Supervisore',
            'grant_view' => '0',
            'grant_update' => '0',
            'grant_delete' => '0',
          ),
          '3' => array(
            'role' => 'Sitcrgreen',
            'grant_view' => '0',
            'grant_update' => '0',
            'grant_delete' => '0',
          ),
          '4' => array(
            'role' => 'Redattore Ras',
            'grant_view' => '0',
            'grant_update' => '0',
            'grant_delete' => '0',
          ),
          '5' => array(
            'role' => 'grafici',
            'grant_view' => '0',
            'grant_update' => '0',
            'grant_delete' => '0',
          ),
          '6' => array(
            'role' => 'Funzionario Regionale',
            'grant_view' => '0',
            'grant_update' => '0',
            'grant_delete' => '0',
          ),
          '7' => array(
            'role' => 'Data entry Associazione',
            'grant_view' => '0',
            'grant_update' => '0',
            'grant_delete' => '0',
          ),
          '8' => array(
            'role' => 'Beta tester',
            'grant_view' => '0',
            'grant_update' => '0',
            'grant_delete' => '0',
          ),
          '9' => array(
            'role' => 'Abbila',
            'grant_view' => '0',
            'grant_update' => '0',
            'grant_delete' => '0',
          ),
          '10' => array(
            'role' => 'authenticated user',
            'rid' => '2',
            'grant_view' => '0',
            'grant_update' => '0',
            'grant_delete' => '0',
          ),
          '11' => array(
            'role' => 'anonymous user',
            'rid' => '1',
            'grant_view' => '0',
            'grant_update' => '0',
            'grant_delete' => '0',
          ),
          '12' => array(
            'role' => 'author',
            'rid' => '-1',
            'grant_view' => '1',
            'grant_update' => '1',
            'grant_delete' => '0',
          ),
          '13' => array(
            'role' => 'Operatore',
            'grant_view' => '1',
            'grant_update' => '1',
            'grant_delete' => '0',
          ),
          '14' => array(
            'role' => 'manager',
            'grant_view' => '1',
            'grant_update' => '1',
            'grant_delete' => '0',
          ),
          '15' => array(
            'role' => 'Community manager',
            'grant_view' => '1',
            'grant_update' => '1',
            'grant_delete' => '0',
          ),
          '16' => array(
            'role' => 'admin',
            'grant_view' => '1',
            'grant_update' => '1',
            'grant_delete' => '1',
          ),
        ),
      ),
      '1344414410' => array(
        'state' => 'prova',
        'weight' => '0',
        'sysid' => '0',
        'status' => '0',
        'module' => 'segnalazione_online_workflow',
        'ref' => '1344414410',
        'access' => array(),
      ),
      '10' => array(
        'state' => 'Attribuita',
        'weight' => '1',
        'sysid' => '0',
        'status' => '1',
        'module' => 'segnalazione_online_workflow',
        'ref' => '10',
        'access' => array(
          '0' => array(
            'role' => 'Unica',
            'grant_view' => '0',
            'grant_update' => '0',
            'grant_delete' => '0',
          ),
          '1' => array(
            'role' => 'Unibo',
            'grant_view' => '0',
            'grant_update' => '0',
            'grant_delete' => '0',
          ),
          '2' => array(
            'role' => 'Supervisore',
            'grant_view' => '0',
            'grant_update' => '0',
            'grant_delete' => '0',
          ),
          '3' => array(
            'role' => 'Sitcrgreen',
            'grant_view' => '0',
            'grant_update' => '0',
            'grant_delete' => '0',
          ),
          '4' => array(
            'role' => 'Redattore Ras',
            'grant_view' => '0',
            'grant_update' => '0',
            'grant_delete' => '0',
          ),
          '5' => array(
            'role' => 'grafici',
            'grant_view' => '0',
            'grant_update' => '0',
            'grant_delete' => '0',
          ),
          '6' => array(
            'role' => 'Funzionario Regionale',
            'grant_view' => '0',
            'grant_update' => '0',
            'grant_delete' => '0',
          ),
          '7' => array(
            'role' => 'Data entry Associazione',
            'grant_view' => '0',
            'grant_update' => '0',
            'grant_delete' => '0',
          ),
          '8' => array(
            'role' => 'Abbila',
            'grant_view' => '0',
            'grant_update' => '0',
            'grant_delete' => '0',
          ),
          '9' => array(
            'role' => 'authenticated user',
            'rid' => '2',
            'grant_view' => '0',
            'grant_update' => '0',
            'grant_delete' => '0',
          ),
          '10' => array(
            'role' => 'anonymous user',
            'rid' => '1',
            'grant_view' => '0',
            'grant_update' => '0',
            'grant_delete' => '0',
          ),
          '11' => array(
            'role' => 'author',
            'rid' => '-1',
            'grant_view' => '1',
            'grant_update' => '0',
            'grant_delete' => '0',
          ),
          '12' => array(
            'role' => 'Operatore',
            'grant_view' => '1',
            'grant_update' => '0',
            'grant_delete' => '0',
          ),
          '13' => array(
            'role' => 'manager',
            'grant_view' => '1',
            'grant_update' => '0',
            'grant_delete' => '0',
          ),
          '14' => array(
            'role' => 'Community manager',
            'grant_view' => '1',
            'grant_update' => '1',
            'grant_delete' => '0',
          ),
          '15' => array(
            'role' => 'Beta tester',
            'grant_view' => '1',
            'grant_update' => '0',
            'grant_delete' => '0',
          ),
          '16' => array(
            'role' => 'admin',
            'grant_view' => '1',
            'grant_update' => '1',
            'grant_delete' => '1',
          ),
        ),
      ),
      '11' => array(
        'state' => 'In Lavorazione',
        'weight' => '3',
        'sysid' => '0',
        'status' => '1',
        'module' => 'segnalazione_online_workflow',
        'ref' => '11',
        'access' => array(
          '0' => array(
            'role' => 'Unica',
            'grant_view' => '0',
            'grant_update' => '0',
            'grant_delete' => '0',
          ),
          '1' => array(
            'role' => 'Unibo',
            'grant_view' => '0',
            'grant_update' => '0',
            'grant_delete' => '0',
          ),
          '2' => array(
            'role' => 'Supervisore',
            'grant_view' => '0',
            'grant_update' => '0',
            'grant_delete' => '0',
          ),
          '3' => array(
            'role' => 'Sitcrgreen',
            'grant_view' => '0',
            'grant_update' => '0',
            'grant_delete' => '0',
          ),
          '4' => array(
            'role' => 'Redattore Ras',
            'grant_view' => '0',
            'grant_update' => '0',
            'grant_delete' => '0',
          ),
          '5' => array(
            'role' => 'grafici',
            'grant_view' => '0',
            'grant_update' => '0',
            'grant_delete' => '0',
          ),
          '6' => array(
            'role' => 'Funzionario Regionale',
            'grant_view' => '0',
            'grant_update' => '0',
            'grant_delete' => '0',
          ),
          '7' => array(
            'role' => 'Data entry Associazione',
            'grant_view' => '0',
            'grant_update' => '0',
            'grant_delete' => '0',
          ),
          '8' => array(
            'role' => 'Beta tester',
            'grant_view' => '0',
            'grant_update' => '0',
            'grant_delete' => '0',
          ),
          '9' => array(
            'role' => 'Abbila',
            'grant_view' => '0',
            'grant_update' => '0',
            'grant_delete' => '0',
          ),
          '10' => array(
            'role' => 'authenticated user',
            'rid' => '2',
            'grant_view' => '0',
            'grant_update' => '0',
            'grant_delete' => '0',
          ),
          '11' => array(
            'role' => 'anonymous user',
            'rid' => '1',
            'grant_view' => '0',
            'grant_update' => '0',
            'grant_delete' => '0',
          ),
          '12' => array(
            'role' => 'author',
            'rid' => '-1',
            'grant_view' => '1',
            'grant_update' => '0',
            'grant_delete' => '0',
          ),
          '13' => array(
            'role' => 'Operatore',
            'grant_view' => '1',
            'grant_update' => '0',
            'grant_delete' => '0',
          ),
          '14' => array(
            'role' => 'manager',
            'grant_view' => '1',
            'grant_update' => '0',
            'grant_delete' => '0',
          ),
          '15' => array(
            'role' => 'Community manager',
            'grant_view' => '1',
            'grant_update' => '1',
            'grant_delete' => '0',
          ),
          '16' => array(
            'role' => 'admin',
            'grant_view' => '1',
            'grant_update' => '1',
            'grant_delete' => '1',
          ),
        ),
      ),
      '12' => array(
        'state' => 'Risposta inviata',
        'weight' => '4',
        'sysid' => '0',
        'status' => '1',
        'module' => 'segnalazione_online_workflow',
        'ref' => '12',
        'access' => array(
          '0' => array(
            'role' => 'author',
            'rid' => '-1',
            'grant_view' => '0',
            'grant_update' => '0',
            'grant_delete' => '0',
          ),
          '1' => array(
            'role' => 'Unica',
            'grant_view' => '0',
            'grant_update' => '0',
            'grant_delete' => '0',
          ),
          '2' => array(
            'role' => 'Unibo',
            'grant_view' => '0',
            'grant_update' => '0',
            'grant_delete' => '0',
          ),
          '3' => array(
            'role' => 'Supervisore',
            'grant_view' => '0',
            'grant_update' => '0',
            'grant_delete' => '0',
          ),
          '4' => array(
            'role' => 'Sitcrgreen',
            'grant_view' => '0',
            'grant_update' => '0',
            'grant_delete' => '0',
          ),
          '5' => array(
            'role' => 'Redattore Ras',
            'grant_view' => '0',
            'grant_update' => '0',
            'grant_delete' => '0',
          ),
          '6' => array(
            'role' => 'grafici',
            'grant_view' => '0',
            'grant_update' => '0',
            'grant_delete' => '0',
          ),
          '7' => array(
            'role' => 'Funzionario Regionale',
            'grant_view' => '0',
            'grant_update' => '0',
            'grant_delete' => '0',
          ),
          '8' => array(
            'role' => 'Data entry Associazione',
            'grant_view' => '0',
            'grant_update' => '0',
            'grant_delete' => '0',
          ),
          '9' => array(
            'role' => 'Abbila',
            'grant_view' => '0',
            'grant_update' => '0',
            'grant_delete' => '0',
          ),
          '10' => array(
            'role' => 'authenticated user',
            'rid' => '2',
            'grant_view' => '0',
            'grant_update' => '0',
            'grant_delete' => '0',
          ),
          '11' => array(
            'role' => 'anonymous user',
            'rid' => '1',
            'grant_view' => '0',
            'grant_update' => '0',
            'grant_delete' => '0',
          ),
          '12' => array(
            'role' => 'Operatore',
            'grant_view' => '1',
            'grant_update' => '0',
            'grant_delete' => '0',
          ),
          '13' => array(
            'role' => 'manager',
            'grant_view' => '1',
            'grant_update' => '0',
            'grant_delete' => '0',
          ),
          '14' => array(
            'role' => 'Community manager',
            'grant_view' => '1',
            'grant_update' => '1',
            'grant_delete' => '0',
          ),
          '15' => array(
            'role' => 'Beta tester',
            'grant_view' => '1',
            'grant_update' => '0',
            'grant_delete' => '0',
          ),
          '16' => array(
            'role' => 'admin',
            'grant_view' => '1',
            'grant_update' => '1',
            'grant_delete' => '1',
          ),
        ),
      ),
    ),
    'roles' => array(
      'author' => array(
        'name' => 'author',
        'transitions' => array(
          '0' => array(
            'from' => '8',
            'to' => '1344414387',
          ),
        ),
      ),
      'admin' => array(
        'name' => 'admin',
        'transitions' => array(
          '0' => array(
            'from' => '8',
            'to' => '1344414387',
          ),
          '1' => array(
            'from' => '1344414387',
            'to' => '10',
          ),
          '2' => array(
            'from' => '1344414387',
            'to' => '11',
          ),
          '3' => array(
            'from' => '10',
            'to' => '1344414387',
          ),
          '4' => array(
            'from' => '10',
            'to' => '11',
          ),
          '5' => array(
            'from' => '11',
            'to' => '1344414387',
          ),
          '6' => array(
            'from' => '11',
            'to' => '10',
          ),
          '7' => array(
            'from' => '12',
            'to' => '1344414387',
          ),
          '8' => array(
            'from' => '12',
            'to' => '10',
          ),
          '9' => array(
            'from' => '12',
            'to' => '11',
          ),
        ),
      ),
      'Community manager' => array(
        'name' => 'Community manager',
        'transitions' => array(
          '0' => array(
            'from' => '8',
            'to' => '1344414387',
          ),
          '1' => array(
            'from' => '1344414387',
            'to' => '10',
          ),
          '2' => array(
            'from' => '1344414387',
            'to' => '11',
          ),
          '3' => array(
            'from' => '10',
            'to' => '1344414387',
          ),
          '4' => array(
            'from' => '10',
            'to' => '11',
          ),
          '5' => array(
            'from' => '11',
            'to' => '12',
          ),
        ),
      ),
      'manager' => array(
        'name' => 'manager',
        'transitions' => array(
          '0' => array(
            'from' => '10',
            'to' => '1344414387',
          ),
          '1' => array(
            'from' => '10',
            'to' => '11',
          ),
          '2' => array(
            'from' => '11',
            'to' => '12',
          ),
        ),
      ),
    ),
    'types' => array(
      'richiesta' => 'richiesta',
    ),
  );
  $defaults[] = array(
    'name' => 'Risposta Workflow',
    'machine_name' => 'risposta_workflow',
    'tab_roles' => array(
      '0' => 'admin',
      '1' => 'Community manager',
      '2' => 'manager',
      '3' => 'Operatore',
    ),
    'options' => array(
      'comment_log_node' => 1,
      'comment_log_tab' => 1,
      'name_as_title' => 0,
    ),
    'states' => array(
      '13' => array(
        'state' => '(creation)',
        'weight' => '-50',
        'sysid' => '1',
        'status' => '1',
        'module' => 'segnalazione_online_workflow',
        'ref' => '13',
        'access' => array(),
      ),
      '14' => array(
        'state' => 'In elaborazione',
        'weight' => '0',
        'sysid' => '0',
        'status' => '1',
        'module' => 'segnalazione_online_workflow',
        'ref' => '14',
        'access' => array(
          '0' => array(
            'role' => 'author',
            'rid' => '-1',
            'grant_view' => '0',
            'grant_update' => '0',
            'grant_delete' => '0',
          ),
          '1' => array(
            'role' => 'Unica',
            'grant_view' => '0',
            'grant_update' => '0',
            'grant_delete' => '0',
          ),
          '2' => array(
            'role' => 'Unibo',
            'grant_view' => '0',
            'grant_update' => '0',
            'grant_delete' => '0',
          ),
          '3' => array(
            'role' => 'Supervisore',
            'grant_view' => '0',
            'grant_update' => '0',
            'grant_delete' => '0',
          ),
          '4' => array(
            'role' => 'Sitcrgreen',
            'grant_view' => '0',
            'grant_update' => '0',
            'grant_delete' => '0',
          ),
          '5' => array(
            'role' => 'Redattore Ras',
            'grant_view' => '0',
            'grant_update' => '0',
            'grant_delete' => '0',
          ),
          '6' => array(
            'role' => 'grafici',
            'grant_view' => '0',
            'grant_update' => '0',
            'grant_delete' => '0',
          ),
          '7' => array(
            'role' => 'Funzionario Regionale',
            'grant_view' => '0',
            'grant_update' => '0',
            'grant_delete' => '0',
          ),
          '8' => array(
            'role' => 'Data entry Associazione',
            'grant_view' => '0',
            'grant_update' => '0',
            'grant_delete' => '0',
          ),
          '9' => array(
            'role' => 'Beta tester',
            'grant_view' => '0',
            'grant_update' => '0',
            'grant_delete' => '0',
          ),
          '10' => array(
            'role' => 'admin',
            'grant_view' => '0',
            'grant_update' => '0',
            'grant_delete' => '0',
          ),
          '11' => array(
            'role' => 'Abbila',
            'grant_view' => '0',
            'grant_update' => '0',
            'grant_delete' => '0',
          ),
          '12' => array(
            'role' => 'authenticated user',
            'rid' => '2',
            'grant_view' => '0',
            'grant_update' => '0',
            'grant_delete' => '0',
          ),
          '13' => array(
            'role' => 'anonymous user',
            'rid' => '1',
            'grant_view' => '0',
            'grant_update' => '0',
            'grant_delete' => '0',
          ),
          '14' => array(
            'role' => 'Operatore',
            'grant_view' => '1',
            'grant_update' => '1',
            'grant_delete' => '1',
          ),
          '15' => array(
            'role' => 'manager',
            'grant_view' => '1',
            'grant_update' => '1',
            'grant_delete' => '1',
          ),
          '16' => array(
            'role' => 'Community manager',
            'grant_view' => '1',
            'grant_update' => '1',
            'grant_delete' => '1',
          ),
        ),
      ),
      '15' => array(
        'state' => 'Da validare',
        'weight' => '1',
        'sysid' => '0',
        'status' => '1',
        'module' => 'segnalazione_online_workflow',
        'ref' => '15',
        'access' => array(
          '0' => array(
            'role' => 'author',
            'rid' => '-1',
            'grant_view' => '0',
            'grant_update' => '0',
            'grant_delete' => '0',
          ),
          '1' => array(
            'role' => 'Unica',
            'grant_view' => '0',
            'grant_update' => '0',
            'grant_delete' => '0',
          ),
          '2' => array(
            'role' => 'Unibo',
            'grant_view' => '0',
            'grant_update' => '0',
            'grant_delete' => '0',
          ),
          '3' => array(
            'role' => 'Supervisore',
            'grant_view' => '0',
            'grant_update' => '0',
            'grant_delete' => '0',
          ),
          '4' => array(
            'role' => 'Sitcrgreen',
            'grant_view' => '0',
            'grant_update' => '0',
            'grant_delete' => '0',
          ),
          '5' => array(
            'role' => 'Redattore Ras',
            'grant_view' => '0',
            'grant_update' => '0',
            'grant_delete' => '0',
          ),
          '6' => array(
            'role' => 'Operatore',
            'grant_view' => '0',
            'grant_update' => '0',
            'grant_delete' => '0',
          ),
          '7' => array(
            'role' => 'Funzionario Regionale',
            'grant_view' => '0',
            'grant_update' => '0',
            'grant_delete' => '0',
          ),
          '8' => array(
            'role' => 'Data entry Associazione',
            'grant_view' => '0',
            'grant_update' => '0',
            'grant_delete' => '0',
          ),
          '9' => array(
            'role' => 'Beta tester',
            'grant_view' => '0',
            'grant_update' => '0',
            'grant_delete' => '0',
          ),
          '10' => array(
            'role' => 'admin',
            'grant_view' => '0',
            'grant_update' => '0',
            'grant_delete' => '0',
          ),
          '11' => array(
            'role' => 'Abbila',
            'grant_view' => '0',
            'grant_update' => '0',
            'grant_delete' => '0',
          ),
          '12' => array(
            'role' => 'authenticated user',
            'rid' => '2',
            'grant_view' => '0',
            'grant_update' => '0',
            'grant_delete' => '0',
          ),
          '13' => array(
            'role' => 'anonymous user',
            'rid' => '1',
            'grant_view' => '0',
            'grant_update' => '0',
            'grant_delete' => '0',
          ),
          '14' => array(
            'role' => 'manager',
            'grant_view' => '1',
            'grant_update' => '1',
            'grant_delete' => '1',
          ),
          '15' => array(
            'role' => 'grafici',
            'grant_view' => '1',
            'grant_update' => '1',
            'grant_delete' => '1',
          ),
          '16' => array(
            'role' => 'Community manager',
            'grant_view' => '1',
            'grant_update' => '1',
            'grant_delete' => '1',
          ),
        ),
      ),
      '16' => array(
        'state' => 'Inviata',
        'weight' => '3',
        'sysid' => '0',
        'status' => '1',
        'module' => 'segnalazione_online_workflow',
        'ref' => '16',
        'access' => array(
          '0' => array(
            'role' => 'author',
            'rid' => '-1',
            'grant_view' => '0',
            'grant_update' => '0',
            'grant_delete' => '0',
          ),
          '1' => array(
            'role' => 'Unica',
            'grant_view' => '0',
            'grant_update' => '0',
            'grant_delete' => '0',
          ),
          '2' => array(
            'role' => 'Unibo',
            'grant_view' => '0',
            'grant_update' => '0',
            'grant_delete' => '0',
          ),
          '3' => array(
            'role' => 'Supervisore',
            'grant_view' => '0',
            'grant_update' => '0',
            'grant_delete' => '0',
          ),
          '4' => array(
            'role' => 'Sitcrgreen',
            'grant_view' => '0',
            'grant_update' => '0',
            'grant_delete' => '0',
          ),
          '5' => array(
            'role' => 'Redattore Ras',
            'grant_view' => '0',
            'grant_update' => '0',
            'grant_delete' => '0',
          ),
          '6' => array(
            'role' => 'grafici',
            'grant_view' => '0',
            'grant_update' => '0',
            'grant_delete' => '0',
          ),
          '7' => array(
            'role' => 'Funzionario Regionale',
            'grant_view' => '0',
            'grant_update' => '0',
            'grant_delete' => '0',
          ),
          '8' => array(
            'role' => 'Data entry Associazione',
            'grant_view' => '0',
            'grant_update' => '0',
            'grant_delete' => '0',
          ),
          '9' => array(
            'role' => 'admin',
            'grant_view' => '0',
            'grant_update' => '0',
            'grant_delete' => '0',
          ),
          '10' => array(
            'role' => 'Abbila',
            'grant_view' => '0',
            'grant_update' => '0',
            'grant_delete' => '0',
          ),
          '11' => array(
            'role' => 'anonymous user',
            'rid' => '1',
            'grant_view' => '0',
            'grant_update' => '0',
            'grant_delete' => '0',
          ),
          '12' => array(
            'role' => 'Operatore',
            'grant_view' => '1',
            'grant_update' => '0',
            'grant_delete' => '0',
          ),
          '13' => array(
            'role' => 'manager',
            'grant_view' => '1',
            'grant_update' => '0',
            'grant_delete' => '0',
          ),
          '14' => array(
            'role' => 'Community manager',
            'grant_view' => '1',
            'grant_update' => '0',
            'grant_delete' => '0',
          ),
          '15' => array(
            'role' => 'Beta tester',
            'grant_view' => '1',
            'grant_update' => '0',
            'grant_delete' => '0',
          ),
          '16' => array(
            'role' => 'authenticated user',
            'rid' => '2',
            'grant_view' => '1',
            'grant_update' => '0',
            'grant_delete' => '0',
          ),
        ),
      ),
    ),
    'roles' => array(
      'author' => array(
        'name' => 'author',
        'transitions' => array(
          '0' => array(
            'from' => '13',
            'to' => '14',
          ),
        ),
      ),
      'admin' => array(
        'name' => 'admin',
        'transitions' => array(
          '0' => array(
            'from' => '13',
            'to' => '14',
          ),
          '1' => array(
            'from' => '13',
            'to' => '15',
          ),
          '2' => array(
            'from' => '13',
            'to' => '16',
          ),
          '3' => array(
            'from' => '14',
            'to' => '15',
          ),
          '4' => array(
            'from' => '14',
            'to' => '16',
          ),
          '5' => array(
            'from' => '15',
            'to' => '14',
          ),
          '6' => array(
            'from' => '15',
            'to' => '16',
          ),
          '7' => array(
            'from' => '16',
            'to' => '14',
          ),
          '8' => array(
            'from' => '16',
            'to' => '15',
          ),
        ),
      ),
      'Community manager' => array(
        'name' => 'Community manager',
        'transitions' => array(
          '0' => array(
            'from' => '13',
            'to' => '14',
          ),
          '1' => array(
            'from' => '14',
            'to' => '15',
          ),
          '2' => array(
            'from' => '15',
            'to' => '14',
          ),
          '3' => array(
            'from' => '15',
            'to' => '16',
          ),
        ),
      ),
      'manager' => array(
        'name' => 'manager',
        'transitions' => array(
          '0' => array(
            'from' => '13',
            'to' => '14',
          ),
          '1' => array(
            'from' => '14',
            'to' => '15',
          ),
          '2' => array(
            'from' => '15',
            'to' => '14',
          ),
          '3' => array(
            'from' => '15',
            'to' => '16',
          ),
        ),
      ),
      'Operatore' => array(
        'name' => 'Operatore',
        'transitions' => array(
          '0' => array(
            'from' => '14',
            'to' => '15',
          ),
        ),
      ),
    ),
    'types' => array(
      'risposta' => 'risposta',
    ),
  );
  return $defaults;

}
